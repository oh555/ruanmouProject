const AV = getApp().AV;

class Article extends AV.Object {
  get title() {
    return this.get('title');
  }
  set title(value) {
    this.set('title', value);
  }
  get content() {
    return this.get('content');
  }
  set content(value) {
    this.set('content', value);
  }
  get thumbImg() {
    return this.get('thumbImg');
  }
  set thumbImg(value) {
    this.set('thumbImg', value);
  }
  get imageList() {
    return this.get('imageList');
  }
  set imageList(value) {
    this.set('imageList', value);
  }
  get price() {
    return this.get('price');
  }
  set price(value) {
    this.set('price', value);
  }
}
AV.Object.register(Article, 'Article');
module.exports = Article;
