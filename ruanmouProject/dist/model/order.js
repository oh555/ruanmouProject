'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AV = getApp().AV;

var Order = function (_AV$Object) {
  _inherits(Order, _AV$Object);

  function Order() {
    _classCallCheck(this, Order);

    return _possibleConstructorReturn(this, (Order.__proto__ || Object.getPrototypeOf(Order)).apply(this, arguments));
  }

  _createClass(Order, [{
    key: 'tradeId',
    get: function get() {
      return this.get('tradeId');
    },
    set: function set(value) {
      this.set('tradeId', value);
    }
  }, {
    key: 'amount',
    get: function get() {
      return this.get('amount');
    },
    set: function set(value) {
      this.set('amount', value);
    }
  }, {
    key: 'user',
    get: function get() {
      return this.get('user');
    },
    set: function set(value) {
      this.set('user', value);
    }
  }, {
    key: 'productDescription',
    get: function get() {
      return this.get('productDescription');
    },
    set: function set(value) {
      this.set('productDescription', value);
    }
  }, {
    key: 'Article',
    get: function get() {
      return this.get('Article');
    },
    set: function set(value) {
      this.set('Article', value);
    }
  }, {
    key: 'status',
    get: function get() {
      return this.get('status');
    },
    set: function set(value) {
      this.set('status', value);
    }
  }, {
    key: 'ip',
    get: function get() {
      return this.get('ip');
    },
    set: function set(value) {
      this.set('ip', value);
    }
  }, {
    key: 'tradeType',
    get: function get() {
      return this.get('tradeType');
    },
    set: function set(value) {
      this.set('tradeType', value);
    }
  }, {
    key: 'prepayId',
    get: function get() {
      return this.get('prepayId');
    },
    set: function set(value) {
      this.set('prepayId', value);
    }
  }]);

  return Order;
}(AV.Object);

AV.Object.register(Order);
module.exports = Order;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9yZGVyLmpzIl0sIm5hbWVzIjpbIkFWIiwiZ2V0QXBwIiwiT3JkZXIiLCJnZXQiLCJ2YWx1ZSIsInNldCIsIk9iamVjdCIsInJlZ2lzdGVyIiwibW9kdWxlIiwiZXhwb3J0cyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLElBQU1BLEtBQUtDLFNBQVNELEVBQXBCOztJQUNNRSxLOzs7Ozs7Ozs7Ozt3QkFDVTtBQUNaLGFBQU8sS0FBS0MsR0FBTCxDQUFTLFNBQVQsQ0FBUDtBQUNELEs7c0JBQ1dDLEssRUFBTztBQUNqQixXQUFLQyxHQUFMLENBQVMsU0FBVCxFQUFvQkQsS0FBcEI7QUFDRDs7O3dCQUNZO0FBQ1gsYUFBTyxLQUFLRCxHQUFMLENBQVMsUUFBVCxDQUFQO0FBQ0QsSztzQkFDVUMsSyxFQUFPO0FBQ2hCLFdBQUtDLEdBQUwsQ0FBUyxRQUFULEVBQW1CRCxLQUFuQjtBQUNEOzs7d0JBQ1U7QUFDVCxhQUFPLEtBQUtELEdBQUwsQ0FBUyxNQUFULENBQVA7QUFDRCxLO3NCQUNRQyxLLEVBQU87QUFDZCxXQUFLQyxHQUFMLENBQVMsTUFBVCxFQUFpQkQsS0FBakI7QUFDRDs7O3dCQUN3QjtBQUN2QixhQUFPLEtBQUtELEdBQUwsQ0FBUyxvQkFBVCxDQUFQO0FBQ0QsSztzQkFDc0JDLEssRUFBTztBQUM1QixXQUFLQyxHQUFMLENBQVMsb0JBQVQsRUFBK0JELEtBQS9CO0FBQ0Q7Ozt3QkFDYTtBQUNaLGFBQU8sS0FBS0QsR0FBTCxDQUFTLFNBQVQsQ0FBUDtBQUNELEs7c0JBQ1dDLEssRUFBTztBQUNqQixXQUFLQyxHQUFMLENBQVMsU0FBVCxFQUFvQkQsS0FBcEI7QUFDRDs7O3dCQUNZO0FBQ1gsYUFBTyxLQUFLRCxHQUFMLENBQVMsUUFBVCxDQUFQO0FBQ0QsSztzQkFDVUMsSyxFQUFPO0FBQ2hCLFdBQUtDLEdBQUwsQ0FBUyxRQUFULEVBQW1CRCxLQUFuQjtBQUNEOzs7d0JBQ1E7QUFDUCxhQUFPLEtBQUtELEdBQUwsQ0FBUyxJQUFULENBQVA7QUFDRCxLO3NCQUNNQyxLLEVBQU87QUFDWixXQUFLQyxHQUFMLENBQVMsSUFBVCxFQUFlRCxLQUFmO0FBQ0Q7Ozt3QkFDZTtBQUNkLGFBQU8sS0FBS0QsR0FBTCxDQUFTLFdBQVQsQ0FBUDtBQUNELEs7c0JBQ2FDLEssRUFBTztBQUNuQixXQUFLQyxHQUFMLENBQVMsV0FBVCxFQUFzQkQsS0FBdEI7QUFDRDs7O3dCQUNjO0FBQ2IsYUFBTyxLQUFLRCxHQUFMLENBQVMsVUFBVCxDQUFQO0FBQ0QsSztzQkFDWUMsSyxFQUFPO0FBQ2xCLFdBQUtDLEdBQUwsQ0FBUyxVQUFULEVBQXFCRCxLQUFyQjtBQUNEOzs7O0VBdERpQkosR0FBR00sTTs7QUF3RHZCTixHQUFHTSxNQUFILENBQVVDLFFBQVYsQ0FBbUJMLEtBQW5CO0FBQ0FNLE9BQU9DLE9BQVAsR0FBaUJQLEtBQWpCIiwiZmlsZSI6Im9yZGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgQVYgPSBnZXRBcHAoKS5BVjtcclxuY2xhc3MgT3JkZXIgZXh0ZW5kcyBBVi5PYmplY3Qge1xyXG4gIGdldCB0cmFkZUlkKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuZ2V0KCd0cmFkZUlkJyk7XHJcbiAgfVxyXG4gIHNldCB0cmFkZUlkKHZhbHVlKSB7XHJcbiAgICB0aGlzLnNldCgndHJhZGVJZCcsIHZhbHVlKTtcclxuICB9XHJcbiAgZ2V0IGFtb3VudCgpIHtcclxuICAgIHJldHVybiB0aGlzLmdldCgnYW1vdW50Jyk7XHJcbiAgfVxyXG4gIHNldCBhbW91bnQodmFsdWUpIHtcclxuICAgIHRoaXMuc2V0KCdhbW91bnQnLCB2YWx1ZSk7XHJcbiAgfVxyXG4gIGdldCB1c2VyKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuZ2V0KCd1c2VyJyk7XHJcbiAgfVxyXG4gIHNldCB1c2VyKHZhbHVlKSB7XHJcbiAgICB0aGlzLnNldCgndXNlcicsIHZhbHVlKTtcclxuICB9XHJcbiAgZ2V0IHByb2R1Y3REZXNjcmlwdGlvbigpIHtcclxuICAgIHJldHVybiB0aGlzLmdldCgncHJvZHVjdERlc2NyaXB0aW9uJyk7XHJcbiAgfVxyXG4gIHNldCBwcm9kdWN0RGVzY3JpcHRpb24odmFsdWUpIHtcclxuICAgIHRoaXMuc2V0KCdwcm9kdWN0RGVzY3JpcHRpb24nLCB2YWx1ZSk7XHJcbiAgfVxyXG4gIGdldCBBcnRpY2xlKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuZ2V0KCdBcnRpY2xlJyk7XHJcbiAgfVxyXG4gIHNldCBBcnRpY2xlKHZhbHVlKSB7XHJcbiAgICB0aGlzLnNldCgnQXJ0aWNsZScsIHZhbHVlKTtcclxuICB9XHJcbiAgZ2V0IHN0YXR1cygpIHtcclxuICAgIHJldHVybiB0aGlzLmdldCgnc3RhdHVzJyk7XHJcbiAgfVxyXG4gIHNldCBzdGF0dXModmFsdWUpIHtcclxuICAgIHRoaXMuc2V0KCdzdGF0dXMnLCB2YWx1ZSk7XHJcbiAgfVxyXG4gIGdldCBpcCgpIHtcclxuICAgIHJldHVybiB0aGlzLmdldCgnaXAnKTtcclxuICB9XHJcbiAgc2V0IGlwKHZhbHVlKSB7XHJcbiAgICB0aGlzLnNldCgnaXAnLCB2YWx1ZSk7XHJcbiAgfVxyXG4gIGdldCB0cmFkZVR5cGUoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5nZXQoJ3RyYWRlVHlwZScpO1xyXG4gIH1cclxuICBzZXQgdHJhZGVUeXBlKHZhbHVlKSB7XHJcbiAgICB0aGlzLnNldCgndHJhZGVUeXBlJywgdmFsdWUpO1xyXG4gIH1cclxuICBnZXQgcHJlcGF5SWQoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5nZXQoJ3ByZXBheUlkJyk7XHJcbiAgfVxyXG4gIHNldCBwcmVwYXlJZCh2YWx1ZSkge1xyXG4gICAgdGhpcy5zZXQoJ3ByZXBheUlkJywgdmFsdWUpO1xyXG4gIH1cclxufVxyXG5BVi5PYmplY3QucmVnaXN0ZXIoT3JkZXIpO1xyXG5tb2R1bGUuZXhwb3J0cyA9IE9yZGVyOyJdfQ==