'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AV = getApp().AV;

var Article = function (_AV$Object) {
  _inherits(Article, _AV$Object);

  function Article() {
    _classCallCheck(this, Article);

    return _possibleConstructorReturn(this, (Article.__proto__ || Object.getPrototypeOf(Article)).apply(this, arguments));
  }

  _createClass(Article, [{
    key: 'title',
    get: function get() {
      return this.get('title');
    },
    set: function set(value) {
      this.set('title', value);
    }
  }, {
    key: 'content',
    get: function get() {
      return this.get('content');
    },
    set: function set(value) {
      this.set('content', value);
    }
  }, {
    key: 'thumbImg',
    get: function get() {
      return this.get('thumbImg');
    },
    set: function set(value) {
      this.set('thumbImg', value);
    }
  }, {
    key: 'imageList',
    get: function get() {
      return this.get('imageList');
    },
    set: function set(value) {
      this.set('imageList', value);
    }
  }, {
    key: 'price',
    get: function get() {
      return this.get('price');
    },
    set: function set(value) {
      this.set('price', value);
    }
  }]);

  return Article;
}(AV.Object);

AV.Object.register(Article, 'Article');
module.exports = Article;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFydGljbGUuanMiXSwibmFtZXMiOlsiQVYiLCJnZXRBcHAiLCJBcnRpY2xlIiwiZ2V0IiwidmFsdWUiLCJzZXQiLCJPYmplY3QiLCJyZWdpc3RlciIsIm1vZHVsZSIsImV4cG9ydHMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxJQUFNQSxLQUFLQyxTQUFTRCxFQUFwQjs7SUFFTUUsTzs7Ozs7Ozs7Ozs7d0JBQ1E7QUFDVixhQUFPLEtBQUtDLEdBQUwsQ0FBUyxPQUFULENBQVA7QUFDRCxLO3NCQUNTQyxLLEVBQU87QUFDZixXQUFLQyxHQUFMLENBQVMsT0FBVCxFQUFrQkQsS0FBbEI7QUFDRDs7O3dCQUNhO0FBQ1osYUFBTyxLQUFLRCxHQUFMLENBQVMsU0FBVCxDQUFQO0FBQ0QsSztzQkFDV0MsSyxFQUFPO0FBQ2pCLFdBQUtDLEdBQUwsQ0FBUyxTQUFULEVBQW9CRCxLQUFwQjtBQUNEOzs7d0JBQ2M7QUFDYixhQUFPLEtBQUtELEdBQUwsQ0FBUyxVQUFULENBQVA7QUFDRCxLO3NCQUNZQyxLLEVBQU87QUFDbEIsV0FBS0MsR0FBTCxDQUFTLFVBQVQsRUFBcUJELEtBQXJCO0FBQ0Q7Ozt3QkFDZTtBQUNkLGFBQU8sS0FBS0QsR0FBTCxDQUFTLFdBQVQsQ0FBUDtBQUNELEs7c0JBQ2FDLEssRUFBTztBQUNuQixXQUFLQyxHQUFMLENBQVMsV0FBVCxFQUFzQkQsS0FBdEI7QUFDRDs7O3dCQUNXO0FBQ1YsYUFBTyxLQUFLRCxHQUFMLENBQVMsT0FBVCxDQUFQO0FBQ0QsSztzQkFDU0MsSyxFQUFPO0FBQ2YsV0FBS0MsR0FBTCxDQUFTLE9BQVQsRUFBa0JELEtBQWxCO0FBQ0Q7Ozs7RUE5Qm1CSixHQUFHTSxNOztBQWdDekJOLEdBQUdNLE1BQUgsQ0FBVUMsUUFBVixDQUFtQkwsT0FBbkIsRUFBNEIsU0FBNUI7QUFDQU0sT0FBT0MsT0FBUCxHQUFpQlAsT0FBakIiLCJmaWxlIjoiYXJ0aWNsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IEFWID0gZ2V0QXBwKCkuQVY7XHJcblxyXG5jbGFzcyBBcnRpY2xlIGV4dGVuZHMgQVYuT2JqZWN0IHtcclxuICBnZXQgdGl0bGUoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5nZXQoJ3RpdGxlJyk7XHJcbiAgfVxyXG4gIHNldCB0aXRsZSh2YWx1ZSkge1xyXG4gICAgdGhpcy5zZXQoJ3RpdGxlJywgdmFsdWUpO1xyXG4gIH1cclxuICBnZXQgY29udGVudCgpIHtcclxuICAgIHJldHVybiB0aGlzLmdldCgnY29udGVudCcpO1xyXG4gIH1cclxuICBzZXQgY29udGVudCh2YWx1ZSkge1xyXG4gICAgdGhpcy5zZXQoJ2NvbnRlbnQnLCB2YWx1ZSk7XHJcbiAgfVxyXG4gIGdldCB0aHVtYkltZygpIHtcclxuICAgIHJldHVybiB0aGlzLmdldCgndGh1bWJJbWcnKTtcclxuICB9XHJcbiAgc2V0IHRodW1iSW1nKHZhbHVlKSB7XHJcbiAgICB0aGlzLnNldCgndGh1bWJJbWcnLCB2YWx1ZSk7XHJcbiAgfVxyXG4gIGdldCBpbWFnZUxpc3QoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5nZXQoJ2ltYWdlTGlzdCcpO1xyXG4gIH1cclxuICBzZXQgaW1hZ2VMaXN0KHZhbHVlKSB7XHJcbiAgICB0aGlzLnNldCgnaW1hZ2VMaXN0JywgdmFsdWUpO1xyXG4gIH1cclxuICBnZXQgcHJpY2UoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5nZXQoJ3ByaWNlJyk7XHJcbiAgfVxyXG4gIHNldCBwcmljZSh2YWx1ZSkge1xyXG4gICAgdGhpcy5zZXQoJ3ByaWNlJywgdmFsdWUpO1xyXG4gIH1cclxufVxyXG5BVi5PYmplY3QucmVnaXN0ZXIoQXJ0aWNsZSwgJ0FydGljbGUnKTtcclxubW9kdWxlLmV4cG9ydHMgPSBBcnRpY2xlOyJdfQ==