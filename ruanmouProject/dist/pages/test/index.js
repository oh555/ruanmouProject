'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getApp = getApp(),
    AV = _getApp.AV;

exports.default = Page({
  data: {
    '__code__': {
      readme: ''
    },
    articles: [], result: '', src: '' },
  search: function search() {
    var _this = this;
    var query1 = new AV.Query('Todo');
    // query.get('5b777eb0756571003bd32780').then(res => {
    //   debugger;
    //   console.log(res);
    // });
    var query2 = new AV.Query('Todo');
    query1.contains('title', '软谋');
    query2.contains('content', '下午');
    var query = AV.Query.and(query1, query2);
    query.find().then(function (res) {
      debugger;
      console.log(res);
    });
  },
  update: function update() {
    var todo = AV.Object.createWithoutData('Todo', '5b777d1a808ca40064ef8e76');
    todo.set('content', '测试');
    todo.save().then(function (res) {
      return console.log(res);
    });

    // new AV.Query(Todo)
    //   .first()
    //   .then(function(todo) {
    //     var content = '10:54测试更新';
    //     todo.set('content', content);
    //     return todo.save(null, {
    //       query: new AV.Query(Todo).contains('content', '测试'),
    //     });
    //   })
    //   .then(function(todo) {
    //     // 保存成功
    //     debugger;
    //     console.log('当前todo为：', todo);
    //   })
    //   .catch(function(error) {

    //   });
  },
  del: function del() {
    var todo = AV.Object.createWithoutData('Todo', '5b777eb0756571003bd32780');
    todo.destroy().then(function (res) {
      console.log(res);
    }).catch(function (err) {
      console.log(err);
    });
  },
  add: function add() {
    // var Todo = AV.Object.extend('Todo');
    // // 新建一个 Todo 对象
    // var todo = new Todo();
    // todo.set('title', '工程师周会');
    // todo.set('content', '每周工程师会议，周一下午2点');
    // todo.save().then(
    //   function(todo) {
    //     // 成功保存之后，执行其他逻辑.
    //     console.log('New object created with objectId: ' + todo.id);
    //   },
    //   function(error) {
    //     // 异常处理
    //     console.error(
    //       'Failed to create new object, with error message: ' + error.message,
    //     );
    //   },
    // );
  },
  pay: function pay() {
    AV.Cloud.run('order', {}).then(function (data) {
      delete data.appid;
      console.log(data);

      data.fail = function (err) {
        console.log(err);
      };
      data.success = function (res) {
        console.log(res);
      };
      wx.requestPayment(data);
      // 调用成功，得到成功的应答 data
    }, function (err) {
      // 处理调用失败
      console.log(err);
    });
  },
  onLoad: function onLoad() {
    var _this = this;
    // AV.Cloud.run('getScanCode', {}).then(
    //   function(data) {
    //     console.log(data);
    //     // 调用成功，得到成功的应答 data
    //     _this.setData({ src: data.url });
    //   },
    //   function(err) {
    //     // 处理调用失败
    //   },
    // );
    // PromiseLogin()
    //   .then(res => {
    //     console.log(res);
    //     return Promise.resolve('登陆成功，可以访问');
    //   })
    //   .then(res => console.log(res));
    // let flag = login();
    // if (flag) {
    //   console.log('已登录');
    // } else {
    //   console.log('未登录，拒绝访问');
    // }
  }
});


function PromiseLogin() {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      resolve('user ruanmou');
    }, 3000);
  });
}
function login() {
  setTimeout(function () {
    return true;
  }, 3000);
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluZGV4Lnd4cCJdLCJuYW1lcyI6WyJnZXRBcHAiLCJBViIsImRhdGEiLCJhcnRpY2xlcyIsInJlc3VsdCIsInNyYyIsInNlYXJjaCIsIl90aGlzIiwicXVlcnkxIiwiUXVlcnkiLCJxdWVyeTIiLCJjb250YWlucyIsInF1ZXJ5IiwiYW5kIiwiZmluZCIsInRoZW4iLCJjb25zb2xlIiwibG9nIiwicmVzIiwidXBkYXRlIiwidG9kbyIsIk9iamVjdCIsImNyZWF0ZVdpdGhvdXREYXRhIiwic2V0Iiwic2F2ZSIsImRlbCIsImRlc3Ryb3kiLCJjYXRjaCIsImVyciIsImFkZCIsInBheSIsIkNsb3VkIiwicnVuIiwiYXBwaWQiLCJmYWlsIiwic3VjY2VzcyIsInd4IiwicmVxdWVzdFBheW1lbnQiLCJvbkxvYWQiLCJQcm9taXNlTG9naW4iLCJQcm9taXNlIiwicmVzb2x2ZSIsInJlamVjdCIsInNldFRpbWVvdXQiLCJsb2dpbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7O2NBQWFBLFE7SUFBUEMsRSxXQUFBQSxFOzs7QUFXSkMsUUFBTTtBQUFBO0FBQUE7QUFBQTtBQUFFQyxjQUFVLEVBQVosRUFBZ0JDLFFBQVEsRUFBeEIsRUFBNEJDLEtBQUssRUFBakMsRTtBQUNOQyxRLG9CQUFTO0FBQ1AsUUFBSUMsUUFBUSxJQUFaO0FBQ0EsUUFBSUMsU0FBUyxJQUFJUCxHQUFHUSxLQUFQLENBQWEsTUFBYixDQUFiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFJQyxTQUFTLElBQUlULEdBQUdRLEtBQVAsQ0FBYSxNQUFiLENBQWI7QUFDQUQsV0FBT0csUUFBUCxDQUFnQixPQUFoQixFQUF5QixJQUF6QjtBQUNBRCxXQUFPQyxRQUFQLENBQWdCLFNBQWhCLEVBQTJCLElBQTNCO0FBQ0EsUUFBSUMsUUFBUVgsR0FBR1EsS0FBSCxDQUFTSSxHQUFULENBQWFMLE1BQWIsRUFBcUJFLE1BQXJCLENBQVo7QUFDQUUsVUFBTUUsSUFBTixHQUFhQyxJQUFiLENBQWtCLGVBQU87QUFDdkI7QUFDQUMsY0FBUUMsR0FBUixDQUFZQyxHQUFaO0FBQ0QsS0FIRDtBQUlELEc7QUFDREMsUSxvQkFBUztBQUNQLFFBQUlDLE9BQU9uQixHQUFHb0IsTUFBSCxDQUFVQyxpQkFBVixDQUE0QixNQUE1QixFQUFvQywwQkFBcEMsQ0FBWDtBQUNBRixTQUFLRyxHQUFMLENBQVMsU0FBVCxFQUFvQixJQUFwQjtBQUNBSCxTQUFLSSxJQUFMLEdBQVlULElBQVosQ0FBaUI7QUFBQSxhQUFPQyxRQUFRQyxHQUFSLENBQVlDLEdBQVosQ0FBUDtBQUFBLEtBQWpCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNELEc7QUFDRE8sSyxpQkFBTTtBQUNKLFFBQUlMLE9BQU9uQixHQUFHb0IsTUFBSCxDQUFVQyxpQkFBVixDQUE0QixNQUE1QixFQUFvQywwQkFBcEMsQ0FBWDtBQUNBRixTQUNHTSxPQURILEdBRUdYLElBRkgsQ0FFUSxlQUFPO0FBQ1hDLGNBQVFDLEdBQVIsQ0FBWUMsR0FBWjtBQUNELEtBSkgsRUFLR1MsS0FMSCxDQUtTLGVBQU87QUFDWlgsY0FBUUMsR0FBUixDQUFZVyxHQUFaO0FBQ0QsS0FQSDtBQVFELEc7QUFDREMsSyxpQkFBTTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDRCxHO0FBQ0RDLEssaUJBQU07QUFDSjdCLE9BQUc4QixLQUFILENBQVNDLEdBQVQsQ0FBYSxPQUFiLEVBQXNCLEVBQXRCLEVBQTBCakIsSUFBMUIsQ0FDRSxVQUFTYixJQUFULEVBQWU7QUFDYixhQUFPQSxLQUFLK0IsS0FBWjtBQUNBakIsY0FBUUMsR0FBUixDQUFZZixJQUFaOztBQUVBQSxXQUFLZ0MsSUFBTCxHQUFZLFVBQVNOLEdBQVQsRUFBYztBQUN4QlosZ0JBQVFDLEdBQVIsQ0FBWVcsR0FBWjtBQUNELE9BRkQ7QUFHQTFCLFdBQUtpQyxPQUFMLEdBQWUsVUFBU2pCLEdBQVQsRUFBYztBQUMzQkYsZ0JBQVFDLEdBQVIsQ0FBWUMsR0FBWjtBQUNELE9BRkQ7QUFHQWtCLFNBQUdDLGNBQUgsQ0FBa0JuQyxJQUFsQjtBQUNBO0FBQ0QsS0FiSCxFQWNFLFVBQVMwQixHQUFULEVBQWM7QUFDWjtBQUNBWixjQUFRQyxHQUFSLENBQVlXLEdBQVo7QUFDRCxLQWpCSDtBQW1CRCxHO0FBQ0RVLFEsb0JBQVM7QUFDUCxRQUFJL0IsUUFBUSxJQUFaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDRDs7OztBQUdILFNBQVNnQyxZQUFULEdBQXdCO0FBQ3RCLFNBQU8sSUFBSUMsT0FBSixDQUFZLFVBQUNDLE9BQUQsRUFBVUMsTUFBVixFQUFxQjtBQUN0Q0MsZUFBVyxZQUFNO0FBQ2ZGLGNBQVEsY0FBUjtBQUNELEtBRkQsRUFFRyxJQUZIO0FBR0QsR0FKTSxDQUFQO0FBS0Q7QUFDRCxTQUFTRyxLQUFULEdBQWlCO0FBQ2ZELGFBQVcsWUFBTTtBQUNmLFdBQU8sSUFBUDtBQUNELEdBRkQsRUFFRyxJQUZIO0FBR0QiLCJmaWxlIjoiaW5kZXgud3hwIiwic291cmNlc0NvbnRlbnQiOlsibGV0IHsgQVYgfSA9IGdldEFwcCgpO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG4gIGNvbmZpZzoge1xyXG4gICAgbmF2aWdhdGlvbkJhclRpdGxlVGV4dDogJ+a1i+ivlScsXHJcbiAgICB1c2luZ0NvbXBvbmVudHM6IHtcclxuICAgICAgJ3d4Yy1wYW5lbCc6ICdAbWludWkvd3hjLXBhbmVsJyxcclxuICAgICAgJ3d4Yy1wYW5lbCc6ICdAbWludWkvd3hjLXBhbmVsJyxcclxuICAgICAgJ3d4Yy1lbGlwJzogJ0BtaW51aS93eGMtZWxpcCcsXHJcbiAgICB9LFxyXG4gIH0sXHJcbiAgZGF0YTogeyBhcnRpY2xlczogW10sIHJlc3VsdDogJycsIHNyYzogJycgfSxcclxuICBzZWFyY2goKSB7XHJcbiAgICBsZXQgX3RoaXMgPSB0aGlzO1xyXG4gICAgdmFyIHF1ZXJ5MSA9IG5ldyBBVi5RdWVyeSgnVG9kbycpO1xyXG4gICAgLy8gcXVlcnkuZ2V0KCc1Yjc3N2ViMDc1NjU3MTAwM2JkMzI3ODAnKS50aGVuKHJlcyA9PiB7XHJcbiAgICAvLyAgIGRlYnVnZ2VyO1xyXG4gICAgLy8gICBjb25zb2xlLmxvZyhyZXMpO1xyXG4gICAgLy8gfSk7XHJcbiAgICBsZXQgcXVlcnkyID0gbmV3IEFWLlF1ZXJ5KCdUb2RvJyk7XHJcbiAgICBxdWVyeTEuY29udGFpbnMoJ3RpdGxlJywgJ+i9r+iwiycpO1xyXG4gICAgcXVlcnkyLmNvbnRhaW5zKCdjb250ZW50JywgJ+S4i+WNiCcpO1xyXG4gICAgdmFyIHF1ZXJ5ID0gQVYuUXVlcnkuYW5kKHF1ZXJ5MSwgcXVlcnkyKTtcclxuICAgIHF1ZXJ5LmZpbmQoKS50aGVuKHJlcyA9PiB7XHJcbiAgICAgIGRlYnVnZ2VyO1xyXG4gICAgICBjb25zb2xlLmxvZyhyZXMpO1xyXG4gICAgfSk7XHJcbiAgfSxcclxuICB1cGRhdGUoKSB7XHJcbiAgICB2YXIgdG9kbyA9IEFWLk9iamVjdC5jcmVhdGVXaXRob3V0RGF0YSgnVG9kbycsICc1Yjc3N2QxYTgwOGNhNDAwNjRlZjhlNzYnKTtcclxuICAgIHRvZG8uc2V0KCdjb250ZW50JywgJ+a1i+ivlScpO1xyXG4gICAgdG9kby5zYXZlKCkudGhlbihyZXMgPT4gY29uc29sZS5sb2cocmVzKSk7XHJcblxyXG4gICAgLy8gbmV3IEFWLlF1ZXJ5KFRvZG8pXHJcbiAgICAvLyAgIC5maXJzdCgpXHJcbiAgICAvLyAgIC50aGVuKGZ1bmN0aW9uKHRvZG8pIHtcclxuICAgIC8vICAgICB2YXIgY29udGVudCA9ICcxMDo1NOa1i+ivleabtOaWsCc7XHJcbiAgICAvLyAgICAgdG9kby5zZXQoJ2NvbnRlbnQnLCBjb250ZW50KTtcclxuICAgIC8vICAgICByZXR1cm4gdG9kby5zYXZlKG51bGwsIHtcclxuICAgIC8vICAgICAgIHF1ZXJ5OiBuZXcgQVYuUXVlcnkoVG9kbykuY29udGFpbnMoJ2NvbnRlbnQnLCAn5rWL6K+VJyksXHJcbiAgICAvLyAgICAgfSk7XHJcbiAgICAvLyAgIH0pXHJcbiAgICAvLyAgIC50aGVuKGZ1bmN0aW9uKHRvZG8pIHtcclxuICAgIC8vICAgICAvLyDkv53lrZjmiJDlip9cclxuICAgIC8vICAgICBkZWJ1Z2dlcjtcclxuICAgIC8vICAgICBjb25zb2xlLmxvZygn5b2T5YmNdG9kb+S4uu+8micsIHRvZG8pO1xyXG4gICAgLy8gICB9KVxyXG4gICAgLy8gICAuY2F0Y2goZnVuY3Rpb24oZXJyb3IpIHtcclxuXHJcbiAgICAvLyAgIH0pO1xyXG4gIH0sXHJcbiAgZGVsKCkge1xyXG4gICAgdmFyIHRvZG8gPSBBVi5PYmplY3QuY3JlYXRlV2l0aG91dERhdGEoJ1RvZG8nLCAnNWI3NzdlYjA3NTY1NzEwMDNiZDMyNzgwJyk7XHJcbiAgICB0b2RvXHJcbiAgICAgIC5kZXN0cm95KClcclxuICAgICAgLnRoZW4ocmVzID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhyZXMpO1xyXG4gICAgICB9KVxyXG4gICAgICAuY2F0Y2goZXJyID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhlcnIpO1xyXG4gICAgICB9KTtcclxuICB9LFxyXG4gIGFkZCgpIHtcclxuICAgIC8vIHZhciBUb2RvID0gQVYuT2JqZWN0LmV4dGVuZCgnVG9kbycpO1xyXG4gICAgLy8gLy8g5paw5bu65LiA5LiqIFRvZG8g5a+56LGhXHJcbiAgICAvLyB2YXIgdG9kbyA9IG5ldyBUb2RvKCk7XHJcbiAgICAvLyB0b2RvLnNldCgndGl0bGUnLCAn5bel56iL5biI5ZGo5LyaJyk7XHJcbiAgICAvLyB0b2RvLnNldCgnY29udGVudCcsICfmr4/lkajlt6XnqIvluIjkvJrorq7vvIzlkajkuIDkuIvljYgy54K5Jyk7XHJcbiAgICAvLyB0b2RvLnNhdmUoKS50aGVuKFxyXG4gICAgLy8gICBmdW5jdGlvbih0b2RvKSB7XHJcbiAgICAvLyAgICAgLy8g5oiQ5Yqf5L+d5a2Y5LmL5ZCO77yM5omn6KGM5YW25LuW6YC76L6RLlxyXG4gICAgLy8gICAgIGNvbnNvbGUubG9nKCdOZXcgb2JqZWN0IGNyZWF0ZWQgd2l0aCBvYmplY3RJZDogJyArIHRvZG8uaWQpO1xyXG4gICAgLy8gICB9LFxyXG4gICAgLy8gICBmdW5jdGlvbihlcnJvcikge1xyXG4gICAgLy8gICAgIC8vIOW8guW4uOWkhOeQhlxyXG4gICAgLy8gICAgIGNvbnNvbGUuZXJyb3IoXHJcbiAgICAvLyAgICAgICAnRmFpbGVkIHRvIGNyZWF0ZSBuZXcgb2JqZWN0LCB3aXRoIGVycm9yIG1lc3NhZ2U6ICcgKyBlcnJvci5tZXNzYWdlLFxyXG4gICAgLy8gICAgICk7XHJcbiAgICAvLyAgIH0sXHJcbiAgICAvLyApO1xyXG4gIH0sXHJcbiAgcGF5KCkge1xyXG4gICAgQVYuQ2xvdWQucnVuKCdvcmRlcicsIHt9KS50aGVuKFxyXG4gICAgICBmdW5jdGlvbihkYXRhKSB7XHJcbiAgICAgICAgZGVsZXRlIGRhdGEuYXBwaWRcclxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcclxuXHJcbiAgICAgICAgZGF0YS5mYWlsID0gZnVuY3Rpb24oZXJyKSB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhlcnIpO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgZGF0YS5zdWNjZXNzID0gZnVuY3Rpb24ocmVzKSB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhyZXMpO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgd3gucmVxdWVzdFBheW1lbnQoZGF0YSk7XHJcbiAgICAgICAgLy8g6LCD55So5oiQ5Yqf77yM5b6X5Yiw5oiQ5Yqf55qE5bqU562UIGRhdGFcclxuICAgICAgfSxcclxuICAgICAgZnVuY3Rpb24oZXJyKSB7XHJcbiAgICAgICAgLy8g5aSE55CG6LCD55So5aSx6LSlXHJcbiAgICAgICAgY29uc29sZS5sb2coZXJyKTtcclxuICAgICAgfSxcclxuICAgICk7XHJcbiAgfSxcclxuICBvbkxvYWQoKSB7XHJcbiAgICBsZXQgX3RoaXMgPSB0aGlzO1xyXG4gICAgLy8gQVYuQ2xvdWQucnVuKCdnZXRTY2FuQ29kZScsIHt9KS50aGVuKFxyXG4gICAgLy8gICBmdW5jdGlvbihkYXRhKSB7XHJcbiAgICAvLyAgICAgY29uc29sZS5sb2coZGF0YSk7XHJcbiAgICAvLyAgICAgLy8g6LCD55So5oiQ5Yqf77yM5b6X5Yiw5oiQ5Yqf55qE5bqU562UIGRhdGFcclxuICAgIC8vICAgICBfdGhpcy5zZXREYXRhKHsgc3JjOiBkYXRhLnVybCB9KTtcclxuICAgIC8vICAgfSxcclxuICAgIC8vICAgZnVuY3Rpb24oZXJyKSB7XHJcbiAgICAvLyAgICAgLy8g5aSE55CG6LCD55So5aSx6LSlXHJcbiAgICAvLyAgIH0sXHJcbiAgICAvLyApO1xyXG4gICAgLy8gUHJvbWlzZUxvZ2luKClcclxuICAgIC8vICAgLnRoZW4ocmVzID0+IHtcclxuICAgIC8vICAgICBjb25zb2xlLmxvZyhyZXMpO1xyXG4gICAgLy8gICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoJ+eZu+mZhuaIkOWKn++8jOWPr+S7peiuv+mXricpO1xyXG4gICAgLy8gICB9KVxyXG4gICAgLy8gICAudGhlbihyZXMgPT4gY29uc29sZS5sb2cocmVzKSk7XHJcbiAgICAvLyBsZXQgZmxhZyA9IGxvZ2luKCk7XHJcbiAgICAvLyBpZiAoZmxhZykge1xyXG4gICAgLy8gICBjb25zb2xlLmxvZygn5bey55m75b2VJyk7XHJcbiAgICAvLyB9IGVsc2Uge1xyXG4gICAgLy8gICBjb25zb2xlLmxvZygn5pyq55m75b2V77yM5ouS57ud6K6/6ZeuJyk7XHJcbiAgICAvLyB9XHJcbiAgfSxcclxufTtcclxuXHJcbmZ1bmN0aW9uIFByb21pc2VMb2dpbigpIHtcclxuICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgIHJlc29sdmUoJ3VzZXIgcnVhbm1vdScpO1xyXG4gICAgfSwgMzAwMCk7XHJcbiAgfSk7XHJcbn1cclxuZnVuY3Rpb24gbG9naW4oKSB7XHJcbiAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9LCAzMDAwKTtcclxufSJdfQ==