'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var AV = getApp().AV;
var util = require('../../util/util.js');
exports.default = Page({
  data: {
    '__code__': {
      readme: ''
    },
    articles: [], pageIndex: 0 },
  onClick: function onClick(e) {
    wx.navigateTo({
      url: '../detail/index?id=' + e.target.dataset.id,
      fail: function fail(err) {
        console.log(err);
      }
    });
  },
  onLoad: function onLoad() {
    var _this2 = this;

    var _this = this;
    this.getArticles(0).then(function (articles) {
      _this2.setData({ articles: articles });
    });
  },
  onPullDownRefresh: function onPullDownRefresh() {
    var _this3 = this;

    this.getArticles(0).then(function (articles) {
      _this3.setData({ articles: articles });
    });
  },
  onReachBottom: function onReachBottom() {
    var _this4 = this;

    var pageIndex = this.data.pageIndex;
    var oldArticles = this.data.articles;
    ++pageIndex;

    this.getArticles(pageIndex).then(function (articles) {
      var newArticles = [].concat(_toConsumableArray(oldArticles), _toConsumableArray(articles));
      _this4.setData({ pageIndex: pageIndex, articles: newArticles });
    });
  },
  getArticles: function getArticles(pageIndex) {
    pageIndex = pageIndex ? pageIndex : 0;
    var query = new AV.Query('Article');
    query.limit(1);
    query.skip(1 * pageIndex);
    query.descending('createdAt');
    return query.find().then(function (res) {
      console.log(res);
      var articles = res.map(function (item) {
        // debugger;
        var createdAt = util.formatTime(new Date(item.createdAt));
        var item_json = item.toJSON();
        item_json.createdAt = createdAt;
        return item_json;
      });
      return Promise.resolve(articles);
    }).catch(function (err) {
      console.log(err);
    });
  }
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluZGV4Lnd4cCJdLCJuYW1lcyI6WyJBViIsImdldEFwcCIsInV0aWwiLCJyZXF1aXJlIiwiZGF0YSIsImFydGljbGVzIiwicGFnZUluZGV4Iiwib25DbGljayIsImUiLCJ3eCIsIm5hdmlnYXRlVG8iLCJ1cmwiLCJ0YXJnZXQiLCJkYXRhc2V0IiwiaWQiLCJmYWlsIiwiZXJyIiwiY29uc29sZSIsImxvZyIsIm9uTG9hZCIsIl90aGlzIiwiZ2V0QXJ0aWNsZXMiLCJ0aGVuIiwic2V0RGF0YSIsIm9uUHVsbERvd25SZWZyZXNoIiwib25SZWFjaEJvdHRvbSIsIm9sZEFydGljbGVzIiwibmV3QXJ0aWNsZXMiLCJxdWVyeSIsIlF1ZXJ5IiwibGltaXQiLCJza2lwIiwiZGVzY2VuZGluZyIsImZpbmQiLCJyZXMiLCJtYXAiLCJjcmVhdGVkQXQiLCJmb3JtYXRUaW1lIiwiRGF0ZSIsIml0ZW0iLCJpdGVtX2pzb24iLCJ0b0pTT04iLCJQcm9taXNlIiwicmVzb2x2ZSIsImNhdGNoIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLElBQU1BLEtBQUtDLFNBQVNELEVBQXBCO0FBQ0EsSUFBTUUsT0FBT0MsUUFBUSxvQkFBUixDQUFiOztBQVdFQyxRQUFNO0FBQUE7QUFBQTtBQUFBO0FBQUVDLGNBQVUsRUFBWixFQUFnQkMsV0FBVyxDQUEzQixFO0FBQ05DLFMsbUJBQVFDLEMsRUFBRztBQUNUQyxPQUFHQyxVQUFILENBQWM7QUFDWkMsV0FBSyx3QkFBd0JILEVBQUVJLE1BQUYsQ0FBU0MsT0FBVCxDQUFpQkMsRUFEbEM7QUFFWkMsVUFGWSxnQkFFUEMsR0FGTyxFQUVGO0FBQ1JDLGdCQUFRQyxHQUFSLENBQVlGLEdBQVo7QUFDRDtBQUpXLEtBQWQ7QUFNRCxHO0FBQ0RHLFEsb0JBQVM7QUFBQTs7QUFDUCxRQUFJQyxRQUFRLElBQVo7QUFDQSxTQUFLQyxXQUFMLENBQWlCLENBQWpCLEVBQW9CQyxJQUFwQixDQUF5QixvQkFBWTtBQUNuQyxhQUFLQyxPQUFMLENBQWEsRUFBRWxCLGtCQUFGLEVBQWI7QUFDRCxLQUZEO0FBR0QsRztBQUNEbUIsbUIsK0JBQW9CO0FBQUE7O0FBQ2xCLFNBQUtILFdBQUwsQ0FBaUIsQ0FBakIsRUFBb0JDLElBQXBCLENBQXlCLG9CQUFZO0FBQ25DLGFBQUtDLE9BQUwsQ0FBYSxFQUFFbEIsa0JBQUYsRUFBYjtBQUNELEtBRkQ7QUFHRCxHO0FBQ0RvQixlLDJCQUFnQjtBQUFBOztBQUNkLFFBQUluQixZQUFZLEtBQUtGLElBQUwsQ0FBVUUsU0FBMUI7QUFDQSxRQUFJb0IsY0FBYyxLQUFLdEIsSUFBTCxDQUFVQyxRQUE1QjtBQUNBLE1BQUVDLFNBQUY7O0FBRUEsU0FBS2UsV0FBTCxDQUFpQmYsU0FBakIsRUFBNEJnQixJQUE1QixDQUFpQyxvQkFBWTtBQUMzQyxVQUFJSywyQ0FBa0JELFdBQWxCLHNCQUFrQ3JCLFFBQWxDLEVBQUo7QUFDQSxhQUFLa0IsT0FBTCxDQUFhLEVBQUVqQixvQkFBRixFQUFhRCxVQUFVc0IsV0FBdkIsRUFBYjtBQUNELEtBSEQ7QUFJRCxHO0FBQ0ROLGEsdUJBQVlmLFMsRUFBVztBQUNyQkEsZ0JBQVlBLFlBQVlBLFNBQVosR0FBd0IsQ0FBcEM7QUFDQSxRQUFJc0IsUUFBUSxJQUFJNUIsR0FBRzZCLEtBQVAsQ0FBYSxTQUFiLENBQVo7QUFDQUQsVUFBTUUsS0FBTixDQUFZLENBQVo7QUFDQUYsVUFBTUcsSUFBTixDQUFXLElBQUl6QixTQUFmO0FBQ0FzQixVQUFNSSxVQUFOLENBQWlCLFdBQWpCO0FBQ0EsV0FBT0osTUFDSkssSUFESSxHQUVKWCxJQUZJLENBRUMsZUFBTztBQUNYTCxjQUFRQyxHQUFSLENBQVlnQixHQUFaO0FBQ0EsVUFBSTdCLFdBQVc2QixJQUFJQyxHQUFKLENBQVEsZ0JBQVE7QUFDN0I7QUFDQSxZQUFJQyxZQUFZbEMsS0FBS21DLFVBQUwsQ0FBZ0IsSUFBSUMsSUFBSixDQUFTQyxLQUFLSCxTQUFkLENBQWhCLENBQWhCO0FBQ0EsWUFBSUksWUFBWUQsS0FBS0UsTUFBTCxFQUFoQjtBQUNBRCxrQkFBVUosU0FBVixHQUFzQkEsU0FBdEI7QUFDQSxlQUFPSSxTQUFQO0FBQ0QsT0FOYyxDQUFmO0FBT0EsYUFBT0UsUUFBUUMsT0FBUixDQUFnQnRDLFFBQWhCLENBQVA7QUFDRCxLQVpJLEVBYUp1QyxLQWJJLENBYUUsZUFBTztBQUNaM0IsY0FBUUMsR0FBUixDQUFZRixHQUFaO0FBQ0QsS0FmSSxDQUFQO0FBZ0JEIiwiZmlsZSI6ImluZGV4Lnd4cCIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IEFWID0gZ2V0QXBwKCkuQVY7XG5jb25zdCB1dGlsID0gcmVxdWlyZSgnLi4vLi4vdXRpbC91dGlsLmpzJyk7XG5leHBvcnQgZGVmYXVsdCB7XG4gIGNvbmZpZzoge1xuICAgIG5hdmlnYXRpb25CYXJUaXRsZVRleHQ6ICfkuLvpobUnLFxuICAgIGVuYWJsZVB1bGxEb3duUmVmcmVzaDogdHJ1ZSxcbiAgICB1c2luZ0NvbXBvbmVudHM6IHtcbiAgICAgICd3eGMtcGFuZWwnOiAnQG1pbnVpL3d4Yy1wYW5lbCcsXG4gICAgICAnd3hjLXBhbmVsJzogJ0BtaW51aS93eGMtcGFuZWwnLFxuICAgICAgJ3d4Yy1lbGlwJzogJ0BtaW51aS93eGMtZWxpcCcsXG4gICAgfSxcbiAgfSxcbiAgZGF0YTogeyBhcnRpY2xlczogW10sIHBhZ2VJbmRleDogMCB9LFxuICBvbkNsaWNrKGUpIHtcbiAgICB3eC5uYXZpZ2F0ZVRvKHtcbiAgICAgIHVybDogJy4uL2RldGFpbC9pbmRleD9pZD0nICsgZS50YXJnZXQuZGF0YXNldC5pZCxcbiAgICAgIGZhaWwoZXJyKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKGVycik7XG4gICAgICB9LFxuICAgIH0pO1xuICB9LFxuICBvbkxvYWQoKSB7XG4gICAgbGV0IF90aGlzID0gdGhpcztcbiAgICB0aGlzLmdldEFydGljbGVzKDApLnRoZW4oYXJ0aWNsZXMgPT4ge1xuICAgICAgdGhpcy5zZXREYXRhKHsgYXJ0aWNsZXMgfSk7XG4gICAgfSk7XG4gIH0sXG4gIG9uUHVsbERvd25SZWZyZXNoKCkge1xuICAgIHRoaXMuZ2V0QXJ0aWNsZXMoMCkudGhlbihhcnRpY2xlcyA9PiB7XG4gICAgICB0aGlzLnNldERhdGEoeyBhcnRpY2xlcyB9KTtcbiAgICB9KTtcbiAgfSxcbiAgb25SZWFjaEJvdHRvbSgpIHtcbiAgICBsZXQgcGFnZUluZGV4ID0gdGhpcy5kYXRhLnBhZ2VJbmRleDtcbiAgICBsZXQgb2xkQXJ0aWNsZXMgPSB0aGlzLmRhdGEuYXJ0aWNsZXM7XG4gICAgKytwYWdlSW5kZXg7XG5cbiAgICB0aGlzLmdldEFydGljbGVzKHBhZ2VJbmRleCkudGhlbihhcnRpY2xlcyA9PiB7XG4gICAgICBsZXQgbmV3QXJ0aWNsZXMgPSBbLi4ub2xkQXJ0aWNsZXMsIC4uLmFydGljbGVzXTtcbiAgICAgIHRoaXMuc2V0RGF0YSh7IHBhZ2VJbmRleCwgYXJ0aWNsZXM6IG5ld0FydGljbGVzIH0pO1xuICAgIH0pO1xuICB9LFxuICBnZXRBcnRpY2xlcyhwYWdlSW5kZXgpIHtcbiAgICBwYWdlSW5kZXggPSBwYWdlSW5kZXggPyBwYWdlSW5kZXggOiAwO1xuICAgIGxldCBxdWVyeSA9IG5ldyBBVi5RdWVyeSgnQXJ0aWNsZScpO1xuICAgIHF1ZXJ5LmxpbWl0KDEpO1xuICAgIHF1ZXJ5LnNraXAoMSAqIHBhZ2VJbmRleCk7XG4gICAgcXVlcnkuZGVzY2VuZGluZygnY3JlYXRlZEF0Jyk7XG4gICAgcmV0dXJuIHF1ZXJ5XG4gICAgICAuZmluZCgpXG4gICAgICAudGhlbihyZXMgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZyhyZXMpO1xuICAgICAgICBsZXQgYXJ0aWNsZXMgPSByZXMubWFwKGl0ZW0gPT4ge1xuICAgICAgICAgIC8vIGRlYnVnZ2VyO1xuICAgICAgICAgIGxldCBjcmVhdGVkQXQgPSB1dGlsLmZvcm1hdFRpbWUobmV3IERhdGUoaXRlbS5jcmVhdGVkQXQpKTtcbiAgICAgICAgICBsZXQgaXRlbV9qc29uID0gaXRlbS50b0pTT04oKTtcbiAgICAgICAgICBpdGVtX2pzb24uY3JlYXRlZEF0ID0gY3JlYXRlZEF0O1xuICAgICAgICAgIHJldHVybiBpdGVtX2pzb247XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGFydGljbGVzKTtcbiAgICAgIH0pXG4gICAgICAuY2F0Y2goZXJyID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coZXJyKTtcbiAgICAgIH0pO1xuICB9LFxufTsiXX0=