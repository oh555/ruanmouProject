'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var Article = require('../../model/article.js');
var AV = getApp().AV;
var article = new Article();
exports.default = Page({
  data: {
    '__code__': {
      readme: ''
    },

    title: '',
    imageList: [
      // 'https://developers.weixin.qq.com/miniprogram/dev/image/cat/0.jpg?t=18081511',
      // 'https://developers.weixin.qq.com/miniprogram/dev/image/cat/0.jpg?t=18081511',
      // 'https://developers.weixin.qq.com/miniprogram/dev/image/cat/0.jpg?t=18081511',
      // 'https://developers.weixin.qq.com/miniprogram/dev/image/cat/0.jpg?t=18081511',
      // 'https://developers.weixin.qq.com/miniprogram/dev/image/cat/0.jpg?t=18081511',
      // 'https://developers.weixin.qq.com/miniprogram/dev/image/cat/0.jpg?t=18081511',
    ],
    thumbImgSrc: '',
    priceList: ['免费', 28, 38, 48, 58, 68]
  },
  onLoad: function onLoad() {
    // let app = getApp();
    // var Todo = app.AV.Object.extend('Todo');
    // // 新建一个 Todo 对象
    // var todo = new Todo();
    // todo.set('title', '工程师周会');
    // todo.set('content', '每周工程师会议，周一下午2点');
    // todo.save().then(
    //   function(todo) {
    //     // 成功保存之后，执行其他逻辑.
    //     console.log('New object created with objectId: ' + todo.id);
    //   },
    //   function(error) {
    //     // 异常处理
    //     console.error(
    //       'Failed to create new object, with error message: ' + error.message,
    //     );
    //   },
    // );
  },
  titleOnBlur: function titleOnBlur(e) {
    console.log(e);
    this.setData({ title: e.detail.value });
    article.title = e.detail.value;
  },
  contentConfirm: function contentConfirm(e) {
    // this.setData({})
    article.content = e.detail.value;
  },
  setThumbImg: function setThumbImg() {
    var _this = this;
    wx.chooseImage({
      count: 1, // 默认为9
      sizeType: ['original', 'compressed'], // 指定原图或者压缩图
      sourceType: ['album', 'camera'], // 指定图片来源
      success: function success(res) {
        var tempFilePaths = res.tempFilePaths;
        _this.setData({
          thumbImgSrc: tempFilePaths[0]
        });
        new AV.File(_this.data.title + '-thumbImg', {
          blob: {
            uri: tempFilePaths[0]
          }
        }).save().then(function (res) {
          article.thumbImg = res;
        });
      }
    });
  },
  uploadImgs: function uploadImgs() {
    var _this = this;
    wx.chooseImage({
      count: 9, // 默认为9
      sizeType: ['original', 'compressed'], // 指定原图或者压缩图
      sourceType: ['album', 'camera'], // 指定图片来源
      success: function success(res) {
        var tempFilePaths = res.tempFilePaths;
        _this.setData({
          imageList: tempFilePaths
        });
      }
    });
  },
  send: function send() {
    // debugger;
    var _data = this.data,
        imageList = _data.imageList,
        title = _data.title;

    imageList.map(function (tempFilePaths, index) {
      return function () {
        return new AV.File(title + '-' + index, {
          blob: {
            uri: tempFilePaths
          }
        }).save();
      };
    })
    //https://www.kancloud.cn/kancloud/promises-book/44249   promise顺序调用，只能串行上传
    //https://juejin.im/post/5ab70d3c5188257ddb0f9359
    .reduce(function (m, p) {
      return m.then(function (v) {
        return AV.Promise.all([].concat(_toConsumableArray(v), [p()]));
      });
    }, AV.Promise.resolve([])).then(function (files) {
      var imgUrls = files.map(function (file) {
        return file.url();
      });
      article.imageList = imgUrls;
      return article.save();
    }).then(function (res) {
      return console.log(res);
    }).catch(function (err) {
      return console.log(err);
    });
  }
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluZGV4Lnd4cCJdLCJuYW1lcyI6WyJBcnRpY2xlIiwicmVxdWlyZSIsIkFWIiwiZ2V0QXBwIiwiYXJ0aWNsZSIsImRhdGEiLCJ0aXRsZSIsImltYWdlTGlzdCIsInRodW1iSW1nU3JjIiwicHJpY2VMaXN0Iiwib25Mb2FkIiwidGl0bGVPbkJsdXIiLCJlIiwiY29uc29sZSIsImxvZyIsInNldERhdGEiLCJkZXRhaWwiLCJ2YWx1ZSIsImNvbnRlbnRDb25maXJtIiwiY29udGVudCIsInNldFRodW1iSW1nIiwiX3RoaXMiLCJ3eCIsImNob29zZUltYWdlIiwiY291bnQiLCJzaXplVHlwZSIsInNvdXJjZVR5cGUiLCJzdWNjZXNzIiwicmVzIiwidGVtcEZpbGVQYXRocyIsIkZpbGUiLCJibG9iIiwidXJpIiwic2F2ZSIsInRoZW4iLCJ0aHVtYkltZyIsInVwbG9hZEltZ3MiLCJzZW5kIiwibWFwIiwiaW5kZXgiLCJyZWR1Y2UiLCJtIiwicCIsIlByb21pc2UiLCJhbGwiLCJ2IiwicmVzb2x2ZSIsImltZ1VybHMiLCJmaWxlcyIsImZpbGUiLCJ1cmwiLCJjYXRjaCIsImVyciJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxJQUFNQSxVQUFVQyxRQUFRLHdCQUFSLENBQWhCO0FBQ0EsSUFBTUMsS0FBS0MsU0FBU0QsRUFBcEI7QUFDQSxJQUFJRSxVQUFVLElBQUlKLE9BQUosRUFBZDs7QUFhRUssUUFBTTtBQUFBO0FBQUE7QUFBQTs7QUFDSkMsV0FBTyxFQURIO0FBRUpDLGVBQVc7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOUyxLQUZQO0FBVUpDLGlCQUFhLEVBVlQ7QUFXSkMsZUFBVyxDQUFDLElBQUQsRUFBTyxFQUFQLEVBQVcsRUFBWCxFQUFlLEVBQWYsRUFBbUIsRUFBbkIsRUFBdUIsRUFBdkI7QUFYUCxHO0FBYU5DLFEsb0JBQVM7QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDRCxHO0FBQ0RDLGEsdUJBQVlDLEMsRUFBRztBQUNiQyxZQUFRQyxHQUFSLENBQVlGLENBQVo7QUFDQSxTQUFLRyxPQUFMLENBQWEsRUFBRVQsT0FBT00sRUFBRUksTUFBRixDQUFTQyxLQUFsQixFQUFiO0FBQ0FiLFlBQVFFLEtBQVIsR0FBZ0JNLEVBQUVJLE1BQUYsQ0FBU0MsS0FBekI7QUFDRCxHO0FBQ0RDLGdCLDBCQUFlTixDLEVBQUc7QUFDaEI7QUFDQVIsWUFBUWUsT0FBUixHQUFrQlAsRUFBRUksTUFBRixDQUFTQyxLQUEzQjtBQUNELEc7QUFDREcsYSx5QkFBYztBQUNaLFFBQUlDLFFBQVEsSUFBWjtBQUNBQyxPQUFHQyxXQUFILENBQWU7QUFDYkMsYUFBTyxDQURNLEVBQ0g7QUFDVkMsZ0JBQVUsQ0FBQyxVQUFELEVBQWEsWUFBYixDQUZHLEVBRXlCO0FBQ3RDQyxrQkFBWSxDQUFDLE9BQUQsRUFBVSxRQUFWLENBSEMsRUFHb0I7QUFDakNDLGVBQVMsaUJBQVNDLEdBQVQsRUFBYztBQUNyQixZQUFJQyxnQkFBZ0JELElBQUlDLGFBQXhCO0FBQ0FSLGNBQU1OLE9BQU4sQ0FBYztBQUNaUCx1QkFBYXFCLGNBQWMsQ0FBZDtBQURELFNBQWQ7QUFHQSxZQUFJM0IsR0FBRzRCLElBQVAsQ0FBZVQsTUFBTWhCLElBQU4sQ0FBV0MsS0FBMUIsZ0JBQTRDO0FBQzFDeUIsZ0JBQU07QUFDSkMsaUJBQUtILGNBQWMsQ0FBZDtBQUREO0FBRG9DLFNBQTVDLEVBS0dJLElBTEgsR0FNR0MsSUFOSCxDQU1RLGVBQU87QUFDWDlCLGtCQUFRK0IsUUFBUixHQUFtQlAsR0FBbkI7QUFDRCxTQVJIO0FBU0Q7QUFsQlksS0FBZjtBQW9CRCxHO0FBQ0RRLFksd0JBQWE7QUFDWCxRQUFJZixRQUFRLElBQVo7QUFDQUMsT0FBR0MsV0FBSCxDQUFlO0FBQ2JDLGFBQU8sQ0FETSxFQUNIO0FBQ1ZDLGdCQUFVLENBQUMsVUFBRCxFQUFhLFlBQWIsQ0FGRyxFQUV5QjtBQUN0Q0Msa0JBQVksQ0FBQyxPQUFELEVBQVUsUUFBVixDQUhDLEVBR29CO0FBQ2pDQyxlQUFTLGlCQUFTQyxHQUFULEVBQWM7QUFDckIsWUFBSUMsZ0JBQWdCRCxJQUFJQyxhQUF4QjtBQUNBUixjQUFNTixPQUFOLENBQWM7QUFDWlIscUJBQVdzQjtBQURDLFNBQWQ7QUFHRDtBQVRZLEtBQWY7QUFXRCxHO0FBQ0RRLE0sa0JBQU87QUFDTDtBQURLLGdCQUVzQixLQUFLaEMsSUFGM0I7QUFBQSxRQUVDRSxTQUZELFNBRUNBLFNBRkQ7QUFBQSxRQUVZRCxLQUZaLFNBRVlBLEtBRlo7O0FBR0xDLGNBQ0crQixHQURILENBQ08sVUFBQ1QsYUFBRCxFQUFnQlUsS0FBaEI7QUFBQSxhQUEwQjtBQUFBLGVBQzdCLElBQUlyQyxHQUFHNEIsSUFBUCxDQUFleEIsS0FBZixTQUF3QmlDLEtBQXhCLEVBQWlDO0FBQy9CUixnQkFBTTtBQUNKQyxpQkFBS0g7QUFERDtBQUR5QixTQUFqQyxFQUlHSSxJQUpILEVBRDZCO0FBQUEsT0FBMUI7QUFBQSxLQURQO0FBUUU7QUFDQTtBQVRGLEtBVUdPLE1BVkgsQ0FXSSxVQUFDQyxDQUFELEVBQUlDLENBQUo7QUFBQSxhQUFVRCxFQUFFUCxJQUFGLENBQU87QUFBQSxlQUFLaEMsR0FBR3lDLE9BQUgsQ0FBV0MsR0FBWCw4QkFBbUJDLENBQW5CLElBQXNCSCxHQUF0QixHQUFMO0FBQUEsT0FBUCxDQUFWO0FBQUEsS0FYSixFQVlJeEMsR0FBR3lDLE9BQUgsQ0FBV0csT0FBWCxDQUFtQixFQUFuQixDQVpKLEVBY0daLElBZEgsQ0FjUSxpQkFBUztBQUNiLFVBQUlhLFVBQVVDLE1BQU1WLEdBQU4sQ0FBVTtBQUFBLGVBQVFXLEtBQUtDLEdBQUwsRUFBUjtBQUFBLE9BQVYsQ0FBZDtBQUNBOUMsY0FBUUcsU0FBUixHQUFvQndDLE9BQXBCO0FBQ0EsYUFBTzNDLFFBQVE2QixJQUFSLEVBQVA7QUFDRCxLQWxCSCxFQW1CR0MsSUFuQkgsQ0FtQlE7QUFBQSxhQUFPckIsUUFBUUMsR0FBUixDQUFZYyxHQUFaLENBQVA7QUFBQSxLQW5CUixFQW9CR3VCLEtBcEJILENBb0JTO0FBQUEsYUFBT3RDLFFBQVFDLEdBQVIsQ0FBWXNDLEdBQVosQ0FBUDtBQUFBLEtBcEJUO0FBcUJEIiwiZmlsZSI6ImluZGV4Lnd4cCIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IEFydGljbGUgPSByZXF1aXJlKCcuLi8uLi9tb2RlbC9hcnRpY2xlLmpzJyk7XG5jb25zdCBBViA9IGdldEFwcCgpLkFWO1xubGV0IGFydGljbGUgPSBuZXcgQXJ0aWNsZSgpO1xuZXhwb3J0IGRlZmF1bHQge1xuICBjb25maWc6IHtcbiAgICBuYXZpZ2F0aW9uQmFyVGl0bGVUZXh0OiAn5paw5aKe5YaF5a65JyxcbiAgICB1c2luZ0NvbXBvbmVudHM6IHtcbiAgICAgICd3eGMtZmxleCc6ICdAbWludWkvd3hjLWZsZXgnLFxuICAgICAgJ3d4Yy1pbnB1dCc6ICdAbWludWkvd3hjLWlucHV0JyxcbiAgICAgICd3eGMtYnV0dG9uJzogJ0BtaW51aS93eGMtYnV0dG9uJyxcbiAgICAgICd3eGMtaWNvbic6ICdAbWludWkvd3hjLWljb24nLFxuICAgICAgJ3d4Yy1jYyc6ICdAbWludWkvd3hjLWNjJyxcbiAgICAgICd3eGMtcGFuZWwnOiAnQG1pbnVpL3d4Yy1wYW5lbCcsXG4gICAgfSxcbiAgfSxcbiAgZGF0YToge1xuICAgIHRpdGxlOiAnJyxcbiAgICBpbWFnZUxpc3Q6IFtcbiAgICAgIC8vICdodHRwczovL2RldmVsb3BlcnMud2VpeGluLnFxLmNvbS9taW5pcHJvZ3JhbS9kZXYvaW1hZ2UvY2F0LzAuanBnP3Q9MTgwODE1MTEnLFxuICAgICAgLy8gJ2h0dHBzOi8vZGV2ZWxvcGVycy53ZWl4aW4ucXEuY29tL21pbmlwcm9ncmFtL2Rldi9pbWFnZS9jYXQvMC5qcGc/dD0xODA4MTUxMScsXG4gICAgICAvLyAnaHR0cHM6Ly9kZXZlbG9wZXJzLndlaXhpbi5xcS5jb20vbWluaXByb2dyYW0vZGV2L2ltYWdlL2NhdC8wLmpwZz90PTE4MDgxNTExJyxcbiAgICAgIC8vICdodHRwczovL2RldmVsb3BlcnMud2VpeGluLnFxLmNvbS9taW5pcHJvZ3JhbS9kZXYvaW1hZ2UvY2F0LzAuanBnP3Q9MTgwODE1MTEnLFxuICAgICAgLy8gJ2h0dHBzOi8vZGV2ZWxvcGVycy53ZWl4aW4ucXEuY29tL21pbmlwcm9ncmFtL2Rldi9pbWFnZS9jYXQvMC5qcGc/dD0xODA4MTUxMScsXG4gICAgICAvLyAnaHR0cHM6Ly9kZXZlbG9wZXJzLndlaXhpbi5xcS5jb20vbWluaXByb2dyYW0vZGV2L2ltYWdlL2NhdC8wLmpwZz90PTE4MDgxNTExJyxcbiAgICBdLFxuICAgIHRodW1iSW1nU3JjOiAnJyxcbiAgICBwcmljZUxpc3Q6IFsn5YWN6LS5JywgMjgsIDM4LCA0OCwgNTgsIDY4XSxcbiAgfSxcbiAgb25Mb2FkKCkge1xuICAgIC8vIGxldCBhcHAgPSBnZXRBcHAoKTtcbiAgICAvLyB2YXIgVG9kbyA9IGFwcC5BVi5PYmplY3QuZXh0ZW5kKCdUb2RvJyk7XG4gICAgLy8gLy8g5paw5bu65LiA5LiqIFRvZG8g5a+56LGhXG4gICAgLy8gdmFyIHRvZG8gPSBuZXcgVG9kbygpO1xuICAgIC8vIHRvZG8uc2V0KCd0aXRsZScsICflt6XnqIvluIjlkajkvJonKTtcbiAgICAvLyB0b2RvLnNldCgnY29udGVudCcsICfmr4/lkajlt6XnqIvluIjkvJrorq7vvIzlkajkuIDkuIvljYgy54K5Jyk7XG4gICAgLy8gdG9kby5zYXZlKCkudGhlbihcbiAgICAvLyAgIGZ1bmN0aW9uKHRvZG8pIHtcbiAgICAvLyAgICAgLy8g5oiQ5Yqf5L+d5a2Y5LmL5ZCO77yM5omn6KGM5YW25LuW6YC76L6RLlxuICAgIC8vICAgICBjb25zb2xlLmxvZygnTmV3IG9iamVjdCBjcmVhdGVkIHdpdGggb2JqZWN0SWQ6ICcgKyB0b2RvLmlkKTtcbiAgICAvLyAgIH0sXG4gICAgLy8gICBmdW5jdGlvbihlcnJvcikge1xuICAgIC8vICAgICAvLyDlvILluLjlpITnkIZcbiAgICAvLyAgICAgY29uc29sZS5lcnJvcihcbiAgICAvLyAgICAgICAnRmFpbGVkIHRvIGNyZWF0ZSBuZXcgb2JqZWN0LCB3aXRoIGVycm9yIG1lc3NhZ2U6ICcgKyBlcnJvci5tZXNzYWdlLFxuICAgIC8vICAgICApO1xuICAgIC8vICAgfSxcbiAgICAvLyApO1xuICB9LFxuICB0aXRsZU9uQmx1cihlKSB7XG4gICAgY29uc29sZS5sb2coZSk7XG4gICAgdGhpcy5zZXREYXRhKHsgdGl0bGU6IGUuZGV0YWlsLnZhbHVlIH0pO1xuICAgIGFydGljbGUudGl0bGUgPSBlLmRldGFpbC52YWx1ZTtcbiAgfSxcbiAgY29udGVudENvbmZpcm0oZSkge1xuICAgIC8vIHRoaXMuc2V0RGF0YSh7fSlcbiAgICBhcnRpY2xlLmNvbnRlbnQgPSBlLmRldGFpbC52YWx1ZTtcbiAgfSxcbiAgc2V0VGh1bWJJbWcoKSB7XG4gICAgbGV0IF90aGlzID0gdGhpcztcbiAgICB3eC5jaG9vc2VJbWFnZSh7XG4gICAgICBjb3VudDogMSwgLy8g6buY6K6k5Li6OVxuICAgICAgc2l6ZVR5cGU6IFsnb3JpZ2luYWwnLCAnY29tcHJlc3NlZCddLCAvLyDmjIflrprljp/lm77miJbogIXljovnvKnlm75cbiAgICAgIHNvdXJjZVR5cGU6IFsnYWxidW0nLCAnY2FtZXJhJ10sIC8vIOaMh+WumuWbvueJh+adpea6kFxuICAgICAgc3VjY2VzczogZnVuY3Rpb24ocmVzKSB7XG4gICAgICAgIHZhciB0ZW1wRmlsZVBhdGhzID0gcmVzLnRlbXBGaWxlUGF0aHM7XG4gICAgICAgIF90aGlzLnNldERhdGEoe1xuICAgICAgICAgIHRodW1iSW1nU3JjOiB0ZW1wRmlsZVBhdGhzWzBdLFxuICAgICAgICB9KTtcbiAgICAgICAgbmV3IEFWLkZpbGUoYCR7X3RoaXMuZGF0YS50aXRsZX0tdGh1bWJJbWdgLCB7XG4gICAgICAgICAgYmxvYjoge1xuICAgICAgICAgICAgdXJpOiB0ZW1wRmlsZVBhdGhzWzBdLFxuICAgICAgICAgIH0sXG4gICAgICAgIH0pXG4gICAgICAgICAgLnNhdmUoKVxuICAgICAgICAgIC50aGVuKHJlcyA9PiB7XG4gICAgICAgICAgICBhcnRpY2xlLnRodW1iSW1nID0gcmVzO1xuICAgICAgICAgIH0pO1xuICAgICAgfSxcbiAgICB9KTtcbiAgfSxcbiAgdXBsb2FkSW1ncygpIHtcbiAgICBsZXQgX3RoaXMgPSB0aGlzO1xuICAgIHd4LmNob29zZUltYWdlKHtcbiAgICAgIGNvdW50OiA5LCAvLyDpu5jorqTkuLo5XG4gICAgICBzaXplVHlwZTogWydvcmlnaW5hbCcsICdjb21wcmVzc2VkJ10sIC8vIOaMh+WumuWOn+WbvuaIluiAheWOi+e8qeWbvlxuICAgICAgc291cmNlVHlwZTogWydhbGJ1bScsICdjYW1lcmEnXSwgLy8g5oyH5a6a5Zu+54mH5p2l5rqQXG4gICAgICBzdWNjZXNzOiBmdW5jdGlvbihyZXMpIHtcbiAgICAgICAgdmFyIHRlbXBGaWxlUGF0aHMgPSByZXMudGVtcEZpbGVQYXRocztcbiAgICAgICAgX3RoaXMuc2V0RGF0YSh7XG4gICAgICAgICAgaW1hZ2VMaXN0OiB0ZW1wRmlsZVBhdGhzLFxuICAgICAgICB9KTtcbiAgICAgIH0sXG4gICAgfSk7XG4gIH0sXG4gIHNlbmQoKSB7XG4gICAgLy8gZGVidWdnZXI7XG4gICAgbGV0IHsgaW1hZ2VMaXN0LCB0aXRsZSB9ID0gdGhpcy5kYXRhO1xuICAgIGltYWdlTGlzdFxuICAgICAgLm1hcCgodGVtcEZpbGVQYXRocywgaW5kZXgpID0+ICgpID0+XG4gICAgICAgIG5ldyBBVi5GaWxlKGAke3RpdGxlfS0ke2luZGV4fWAsIHtcbiAgICAgICAgICBibG9iOiB7XG4gICAgICAgICAgICB1cmk6IHRlbXBGaWxlUGF0aHMsXG4gICAgICAgICAgfSxcbiAgICAgICAgfSkuc2F2ZSgpLFxuICAgICAgKVxuICAgICAgLy9odHRwczovL3d3dy5rYW5jbG91ZC5jbi9rYW5jbG91ZC9wcm9taXNlcy1ib29rLzQ0MjQ5ICAgcHJvbWlzZemhuuW6j+iwg+eUqO+8jOWPquiDveS4suihjOS4iuS8oFxuICAgICAgLy9odHRwczovL2p1ZWppbi5pbS9wb3N0LzVhYjcwZDNjNTE4ODI1N2RkYjBmOTM1OVxuICAgICAgLnJlZHVjZShcbiAgICAgICAgKG0sIHApID0+IG0udGhlbih2ID0+IEFWLlByb21pc2UuYWxsKFsuLi52LCBwKCldKSksXG4gICAgICAgIEFWLlByb21pc2UucmVzb2x2ZShbXSksXG4gICAgICApXG4gICAgICAudGhlbihmaWxlcyA9PiB7XG4gICAgICAgIGxldCBpbWdVcmxzID0gZmlsZXMubWFwKGZpbGUgPT4gZmlsZS51cmwoKSk7XG4gICAgICAgIGFydGljbGUuaW1hZ2VMaXN0ID0gaW1nVXJscztcbiAgICAgICAgcmV0dXJuIGFydGljbGUuc2F2ZSgpO1xuICAgICAgfSlcbiAgICAgIC50aGVuKHJlcyA9PiBjb25zb2xlLmxvZyhyZXMpKVxuICAgICAgLmNhdGNoKGVyciA9PiBjb25zb2xlLmxvZyhlcnIpKTtcbiAgfSxcbn07Il19